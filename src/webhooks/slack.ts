import { default as axios } from 'axios';

export const notify = (diff: string, webhookUrl: string, targetName: string) => {
    let message = `New changes detected to the ${targetName} API:
\`\`\`
${diff}
\`\`\``;

    if (message.length > 2000) {
        message = `New changes detected to the ${targetName} API but they are too big to post to slack.`;
    }

    axios.post(webhookUrl, {
        text: message
    }).then(res => {

    }).catch(err => {
        console.error(err);
    });
};