import { existsSync, mkdirSync, writeFileSync } from 'fs';
import { join } from 'path';
import { spawnSync } from 'child_process';

export const ensureRepositoryExists = (root: string, name: string): boolean => {
    const dir = join(root, name);

    if (existsSync(dir)) {
        return false;
    }

    mkdirSync(dir);
    spawnSync('git', ['-C', dir, 'init'], { stdio: 'ignore' });

    return true;
};

export const diff = (schema: string, root: string, name: string, outputFileName: string): string | null => {
    const dir = join(root, name);
    writeFileSync(join(dir, 'schema.graphql'), schema);

    const { stdout } = spawnSync('git', ['-C', dir, 'diff', '--no-color', '--', '.'], { encoding: 'utf8' });

    return stdout.length == 0 ? null : stdout;
};

export const commitChanges = (root: string, name: string): void => {
    const dir = join(root, name);

    spawnSync('git', ['-C', dir, 'add', '.'], { stdio: 'ignore' });
    spawnSync('git', ['-C', dir, 'commit', '-m', new Date().toISOString()], { stdio: 'ignore' });
};