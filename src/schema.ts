import { buildClientSchema, printSchema, getIntrospectionQuery, IntrospectionQuery } from 'graphql';
import axios, { AxiosResponse } from 'axios';

import { Target, Verb } from './config';

interface IntrospectionQueryResponse {
    data: IntrospectionQuery
}

const getIntrospectionResult = async (url: string, verb: Verb, headers: { [key: string]: string }): Promise<IntrospectionQuery | null> => {
    try {
        let response: Promise<AxiosResponse<IntrospectionQueryResponse>>;

        switch (verb) {
            case 'GET':
                response = axios.get(url,
                    {
                        params: {
                            query: getIntrospectionQuery()
                        },
                        headers: headers
                    });
                break;

            case 'POST':
                response = axios.post(url,
                    {
                        query: getIntrospectionQuery(),
                    },
                    {
                        headers: headers
                    });
                break;

            default:
                throw new Error(`Unhandled HTTP Verb: ${verb}`);
        }

        const introspection = await response;

        return introspection.data.data;
    } catch (ex) {
        console.error(ex);
        return null;
    }

};

export const getSchema = async (target: Required<Target>): Promise<string | null> => {
    if (target.type == 'Schema') {
        const response = await axios.get(target.url, {
            headers: target.headers
        });

        return response.data;
    } else if (target.type == 'Introspection') {
        const schema = await getIntrospectionResult(target.url, target.verb, target.headers);
        if (!schema) {
            return null;
        }

        const clientSchema = buildClientSchema(schema);
        const schemaString = printSchema(clientSchema);

        return schemaString;
    } else {
        throw new Error('Unexpected query type.');
    }
};