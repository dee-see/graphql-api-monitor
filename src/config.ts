import { readFileSync } from "fs";

export interface Config {
    dataDirectory?: string,
    webhook?: string,
    targets?: Target[],
    outputFileName?: string
}

export type QueryType = 'Introspection' | 'Schema';
export type Verb = 'POST' | 'GET';

export interface Target {
    name?: string,
    url?: string,
    verb?: Verb,
    type?: QueryType,
    headers?: { [key: string]: string }
}

const injectEnvironmentVariables = (_key: string, value: string | any) => {
    if (!value || typeof value !== 'string') {
        return value;
    }

    let newValue = value;
    for (const match of value.matchAll(/___ENV-(.*?)___/g)) {
        const envVarName = match[1];
        const envVar = process.env[envVarName];
        if (!envVar) {
            throw new Error(`Cannot find value for environment variable ${envVarName}.`);
        }
        newValue = newValue.replace(match[0], envVar);
    }

    return newValue;
};

export const parseConfig = (path: string): Config => {
    const file = readFileSync(path, "utf-8");
    const config = <Config>JSON.parse(file, injectEnvironmentVariables);

    return config;
};

export const fillWithDefaultValues = (target: Target): Required<Target> => {
    if (!target.url || !target.name) {
        throw new Error('Target URL and name are required.');
    }

    return {
        name: target.name,
        url: target.url,
        type: target.type || "Introspection",
        verb: target.verb || "POST",
        headers: target.headers || {}
    };
};